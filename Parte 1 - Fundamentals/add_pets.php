<?php   
    require_once 'lib/functions.php';
    require 'layout/header.php';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        } else {
            $name = '';
        }

        if (isset($_POST['age'])) {
            $age = $_POST['age'];
        } else {
            $age = '';
        }

        if (isset($_POST['weight'])) {
            $weight = $_POST['weight'];
        } else {
            $weight = '';
        }

        if (isset($_POST['image'])) {
            $image = $_POST['image'];
        } else {
            $image = '';
        }

        if (isset($_POST['bio'])) {
            $bio = $_POST['bio'];
        } else {
            $bio = '';
        }

        if (isset($_POST['breed'])) {
            $breed = $_POST['breed'];
        } else {
            $breed = '';
        }

        $newPet= array(
            'name' => $name,
            'age' => $age,
            'weight' => $weight,
            'image' => $image,
            'bio' => $bio,
            'breed' => $breed
        );
        

        //extract ($newPet, EXTR_PREFIX_SAME, "pet");
        //echo "$name, $age, $weight, $image, $bio, $breed \n";
        

        savePet($newPet);

        header('Location: /treinamento-drupal-pedro-franco/index.php');
        
    }


?>

<div class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h1>Add your pets here!</h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <form action="add_pets.php" method="post">
                <div class="form-group">
                    <label for="pet-name" class="control-label">Pet Name</label>
                    <input type="text" name="name" id="pet-name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="pet-age" class="control-label">Age</label>
                    <input type="text" name="age" id="pet-age" class="form-control">
                </div>
                <div class="form-group">
                    <label for="pet-weight" class="control-label">Weight(lbs)</label>
                    <input type="text" name="weight" id="pet-weight" class="form-control">
                </div>
                <div class="form-group">
                    <label for="pet-image" class="control-label">Image</label>
                    <input type="file" name="image" id="pet-image" class="form-control">
                </div>
                <div class="form-group">
                    <label for="pet-bio" class="control-label">Pet Bio</label>
                    <input type="text" name="bio" id="pet-bio" class="form-control">
                </div>
                <div class="form-group">
                    <label for="pet-breed" class="control-label">Breed</label>
                    <input type="text" name="breed" id="pet-breed" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-heart"></span>
                    Add
                </button>
            </form>
        </div>
    </div>
<?php   
 require 'layout/footer.php';
?>