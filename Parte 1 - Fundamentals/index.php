<?php   
  require "lib/config.php";
  require_once "lib/functions.php";
  require 'layout/header.php';

  $pets = getPets(); 
  $number = count($pets);
  $title = "Hello World";     
?>

<div class="jumbotron">
  <div class="container">
    <h1>
      <?php echo strtoupper($title);?>
    </h1>

    <p>The number of puppies is
      <?php echo $number ?>
    </p>

    <p><a class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
  </div>
</div>

<div class="container">
  <div class="row">
    <?php foreach ($pets as $pet) {?>
    <div class="col-md-4 pet-list-item">
      <h2>
        <a href="pet.php?id=<?php echo $pet['id'];?>">
          <?php echo $pet['name']?>
        </a>
      </h2>

      <?php 
        if(array_key_exists('image', $pet) && $pet['image'] != ''){
            echo '<img src="'.$path.'/images/'.$pet['image'].'" class="img-rounded">';
        } else {
            echo '';
        }
      ?>
      <blockquote class="pet-details">
        <span class="label label-info">
          <?php echo $pet['breed'];?></span>
        <?php 
          if(!array_key_exists('age', $pet) || $pet['age'] == ''){
              echo 'Unknown';
          }elseif($pet['age'] != 'hidden'){
              echo $pet['age'].' years';
          }else{
              echo '(Contact the owner)';
          }
        ?>
        <?php echo $pet['weight'] > 8 ? 'I need some exercise': "I'm slim!";?>
      </blockquote>
      <p>
        <?php echo $pet['bio'];?>
      </p>
    </div>
    <?php 
      } 
    ?>
  </div>

<?php   
  require 'layout/footer.php';        
?>