<?php 

    $activePage = basename($_SERVER['PHP_SELF'], ".php");

    $path = "http://localhost/treinamento-drupal-pedro-franco";

    function getConnection(){
        $database = require "lib/config.php";
        $pdo = new PDO($database['database_dsn'], $database['database_user'], $database['database_pass']);

        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }

    function getPets($limit = null){
        $pdo = getConnection();
        $query = 'SELECT * FROM pet';
        if ($limit) {
            $query = $query.'LIMIT :resultLimit';
        }
        $result = $pdo->prepare($query);
        $result->bindParam('resultLimit', $limit, PDO::PARAM_INT);
        $result->execute(); 
        $pets = $result->fetchAll();
        return $pets;
    }

    function getPet($id){
        $pdo = getConnection();

        $query = 'SELECT * FROM pet WHERE id = :idVal';
        $result = $pdo->prepare($query);
        $result->bindParam('idVal', $id);
        $result->execute(); 
        $pet =  $result->fetch();

        if (!$pet) {
            header('Location: /treinamento-drupal-pedro-franco/404.php');
        } 

        return $pet;
    }

    function savePet($pet)
    {
        $pdo = getConnection();

        //$query = "INSERT INTO 'pet'('id','name', 'age', 'weight', 'image', 'bio', 'breed') VALUES (NULL, ':namePet', ':agePet', ':weightPet', ':imagePet', ':bioPet', ':breedPet')";

        $query = "INSERT INTO 'pet'('id','name', 'age', 'weight', 'image', 'bio', 'breed') VALUES ('5', 'Pedro', 5, '34', '', 'fgnfgngfn', 'human')";
        $pdo->exec($query);
        /*$result = $pdo->prepare($query);
        $result->bindParam('namePet', $pet['name']);
        $result->bindParam('agePet', $pet['age']);
        $result->bindParam('weightPet', $pet['weight']);
        $result->bindParam('imagePet', $pet['image']);
        $result->bindParam('bioPet', $pet['bio']);
        $result->bindParam('breedPet', $pet['breed']);
        $result->execute($pet);*/

    }


