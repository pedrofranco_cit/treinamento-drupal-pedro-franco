<?php   
    require_once 'lib/functions.php';
    require 'layout/header.php';
    $id = $_GET['id'];
    $pet = getPet($id);
?>


<div class="jumbotron">
    <div class="container">
        <div class="col-md-10 pet-list-item">
            <h1>
                <?php echo $pet['name']?>
            </h1>
        </div>
    </div>
</div>

<div class="container">
    <?php 
        if(array_key_exists('image', $pet) && $pet['image'] != ''){
            echo '<img src="'.$path.'/images/'.$pet['image'].'" class="img-rounded">';
        } else {
            echo '';
        }
    ?>


    <blockquote class="pet-details">
        <span class="label label-info">
            <?php echo $pet['breed'];?></span>
        <?php 
            if(!array_key_exists('age', $pet) || $pet['age'] == ''){
                echo 'Unknown';
            }elseif($pet['age'] != 'hidden'){
                echo $pet['age'].' years';
            }else{
                echo '(Contact the owner)';
            }
        ?>
        <?php echo $pet['weight'] > 8 ? 'I need some exercise': "I'm slim!";?>
    </blockquote>
    <p>
        <?php echo $pet['bio'];?>
    </p>



<?php   
 require 'layout/footer.php';
?>