<?php

function menu_magic_extra($wildcard){
  $content = array();

  $content['raw_markup'] = array(
    '#type' => 'markup',
    '#markup' => '<p>'.t('Just as magical as "%wildcard"', array('%wildcard' => $wildcard)).'</p>'
  );

  return $content;
}