<?php

class BattleResult
{
    private $winningShip;
    private $losingShip;
    private $usedJediPowers;

    public function __construct(Ship $winningShip, Ship $losingShip = null, $usedJediPowers = null)
    {
        $this->winningShip = $winningShip;
        $this->losingShip = $losingShip;
        $this->usedJediPowers = $usedJediPowers;
    }

    /**
     * Get the value of usedJediPowers
     * @return boolean
     */ 
    public function getUsedJediPowers()
    {
        return $this->usedJediPowers;
    }

    /**
     * Get losingShip
     * @return Ship/null
     */ 
    public function getLosingShip()
    {
        return $this->losingShip;
    }

    /**
     * Get winningShip
     * @return Ship/null
     */ 
    public function getWinningShip()
    {
        return $this->winningShip;
    }
    
    public function isThereAWinner()
    {
        return $this->winningShip !== null;
    }
}